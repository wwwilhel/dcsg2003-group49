# Week 9: New Architecture

We make 3 new servers with the same flavor as db1. 

![](images/fig-9-1.png) \
**Fig 9.1:** *3 new servers set up*

I also gave the servers bad names at first by accident, so this
was changed with the openstack interface, and in the CLI with
this command (This seemed to cause some issues):

```sh
hostname server3
```


## 9.0: Clock Synchronization

We use NTP, a common tool for synchronizing clocks between
virtual machines (this has to be done for all three servers):

```sh
apt-get update && apt-get install -y ntpdate
ntpdate -b ntp.justervesenet.no
```

And since this has to be maintained regularly, it's good to put
it in the crontab, too:

```sh
*/10 * * * * root ntpdate -b ntp.justervesenet.no
```


## 9.1: Installing CockroachDB

We have to install CockroachDB for all three servers:

```sh
wget https://binaries.cockroachdb.com/cockroach-v23.1.13.linux-amd64.tgz  # Download
tar xzf cockroach-v23.1.13.linux-amd64.tgz                                # Extract
cp cockroach-v23.1.13.linux-amd64/cockroach /usr/local/bin                # Move to PATH
mkdir /bfdata      # Make bookface data directory
cockroach version  # Check that cockroach is installed
```

![](images/fig-9-2.png) \
**Fig 9.2:** *A successful cockroachDB installation*


## 9.3: Starting CockroachDB

And then to add this script to each server so it'll be easy to
start whenever we need to. Note that the option `-join` requires
the IPs of all 3 servers, and the option `--advertise-addr`
requires the IP of the server running the command.

Note *also* also the `--max-offset` option, which is 500ms by
default. We increase it right now to increase tolerance. This
*will* make some operations take a little longer, though. Feel
free to experiment.

Then we should go ahead and run the script, too, on all three
servers.

```sh
#!/bin/bash

echo "Starting cockroach up again on server[...]"

cockroach start \
    --insecure \
    --store=/bfdata \
    --listen-addr=0.0.0.0:26257 \
    --http-addr=0.0.0.0:8080 \
    --background \
    --join=192.168.131.131:26257,192.168.128.132:26257,192.168.132.166:26257 \
    --advertise-addr=CURRENT_IP:26257 \
    --max-offset=1500ms
```

![](images/fig-9-3.png) \
**Fig 9.3:** *A successful cockroach start*


## 9.3: Initializing CockroachDB

This one we only need to do for one of the servers. 

Now we run this command; this, we only need to do on one of the
servers:

```sh
cockroach init --insecure --host=CURRENT_IP:26257 
```

This should allow us to see the dashboard from a web browser,
but I can't get it to work, and besides can't see how it would.
The IPs of our servers are all private. 

```
http://SERVER_IP:8080
```

FoxyProxy is supposed to have some effect here, but I don't know
what it would be: 

> "( husk bruk at FoxyProxy for å kunne se noe på "Innsiden" av
> skyen deres )"

I'd like to ask for help on this point.


## 9.4: Configuring the Database

This only needs to be done for one server. Now we do some stuff
through the SQL interface:

```sh
cockroach sql --insecure --host=localhost:26257 
```

```sql
CREATE DATABASE bf;
CREATE USER bfuser;
GRANT ALL ON DATABASE bf TO bfuser;
USE bf;
```

Make tables:

```sql
CREATE TABLE users (
userid INT NOT NULL DEFAULT unique_rowid(),
name STRING(50) NULL,
picture STRING(300) NULL,
status STRING(10) NULL,
posts INT NULL,
comments INT NULL,
lastpostdate TIMESTAMP NULL DEFAULT now():::TIMESTAMP,
createdate TIMESTAMP NULL DEFAULT now():::TIMESTAMP,
FAMILY "primary" (userid, posts, comments, lastpostdate),
FAMILY "secondary" (name, picture,status, createdate)
);
```

```sql
CREATE TABLE posts (
postid INT NOT NULL DEFAULT unique_rowid(),
userid INT NOT NULL,
text STRING(300) NULL,
name STRING(150) NULL,
postdate TIMESTAMP NULL DEFAULT now():::TIMESTAMP,
INDEX posts_auto_index_posts_users_fk (userid ASC),
FAMILY "primary" (postid, userid, text, name, postdate)
);
```

```sql
CREATE TABLE comments (
commentid INT NOT NULL DEFAULT unique_rowid(),
userid INT NOT NULL,
postid INT NOT NULL,
text STRING(300) NULL,
postdate TIMESTAMP NULL DEFAULT now():::TIMESTAMP,
INDEX comments_userid_idx (userid ASC),
INDEX comments_postid_idx (postid ASC),
FAMILY "primary" (commentid, userid, postid, text, postdate)
);
```

```sql
CREATE table config ( key STRING(100), value STRING(500) );
```

```sql
create index on users (name);
create index on users (userid);
create index on users (lastpostdate);
create index on posts (userid);
create index on posts (postdate);
create index on comments (postid,postdate);
create index on comments (postid);
```


## 9.5: Starting CockroachDB Automatically on Server Start

This is good for when a server goes down. Getting the server up
again is easy enough with openstack.

We make a file in each server `/etc/systemd/system/bookface.service` with this
content:

```
[Unit]
Description=Bookface script

[Service]
Type=simple
KillMode=process
ExecStart=/root/bookface_start.sh

[Install]
WantedBy=multi-user.target 
```

This file instructs to run a script `/root/bookface_start.sh`,
which is the same as the "cockroach start" script defined
earlier. Could be a hard link from one to the other.

Also run this command:

```sh
systemctl enable bookface
```

The file `/var/log/syslog` logs a bunch of things, including
stdout of this, so we can check for a process which ran without
supervision such as this, by inspecting the contents of this
file.

> https://linuxhandbook.com/syslog-guide/


## You Can Also Check the Status Without the Dashboard

```sh
curl -s http://localhost:8080/_status/vars 
```

This command outputs a *lot* of information about the cluster,
which can be navigated with unix tools such as grep. Info on how
to do that well here:

[https://www.cockroachlabs.com/docs/stable/monitoring-and-alerting.html](https://www.cockroachlabs.com/docs/stable/monitoring-and-alerting.html)


## Another Thing

Also here's a script to check if cockroach is running on all
servers:

```sh
#!/bin/bash
echo "-- Checking server1"; ssh 192.168.131.131 ps aux | grep cockroach
echo "-- Checking server2"; ssh 192.168.128.132 ps aux | grep cockroach
echo "-- Checking server3"; ssh 192.168.132.166 ps aux | grep cockroach
```
