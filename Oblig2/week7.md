# Oblig 2

# Week 7: Caching

## 7.1: Setting up a Memcache Server

This can be done with Docker, but we make a new server instance
through openstack. We log into the server and run:

```sh 
apt update && apt install memcached
service memcached restart
```

![images/fig-7-1.png](images/fig-7-1.png) \
**Fig 7.1:** *We try with 1 VCPU and 2GB RAM.*


### Letting Bookface Use the Memcache

We install php-memcached to our web servers:
(I think it saying `php-memcache` in the weekly tutorial is a
typo, but I can't be sure...)

```sh
apt-get update && apt-get install php-memcached
```

And we add these lines to the `config.php`s:

```
$memcache_server = "MEMCACHE_IP";
$memcache_enabled = 1;
$memcache_enabled_pictures = 1; 
```

We see that it works by curling the balancer and looking for the
memcache comments:

![images/fig-7-2.png](images/fig-7-1.png) \
**Fig 7.2:** *Memcache works.*

Also of note, is that if the memcache server is shut off, the
service will still work.


### Results:

After testing a little with this command:

```sh
curl -s http://[ENDPOINT] | grep -i memcache
```

It seems the cache isn't doing much, and the cached elements
are overwritten before having any effect. May be we can get it to work by
changing the memcached config, increase memory, change VM size
or something.

That's it.
