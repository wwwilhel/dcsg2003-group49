# Week 5: Docker

## 5.1: Make a Docker VM

We make a docker VM and install docker on it based on the
instructions in link below. The instructions can be automated
with the script below for later use.

[https://docs.docker.com/install/linux/docker-ce/ubuntu/](https://docs.docker.com/install/linux/docker-ce/ubuntu/)


```shell
#!/bin/bash
# A script to install docker. Appropriated from:
# https://docs.docker.com/engine/install/ubuntu/

sudo apt-get remove \
    docker.io docker-doc docker-compose docker-compose-v2 \
    podman-docker containerd runc

# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL \
    https://download.docker.com/linux/ubuntu/gpg \
    -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
"deb [arch=$(dpkg --print-architecture) \
signed-by=/etc/apt/keyrings/docker.asc] \
https://download.docker.com/linux/ubuntu \
$(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install the Docker Packages
sudo apt-get install \
    docker-ce docker-ce-cli containerd.io docker-buildx-plugin \
    docker-compose-plugin
```

## 5.2: Optimizations

### Frontpage Limit

The config files in www1/www2 can be changed to accept fewer
frontpage pictures, decreasig strain. Changed with variable
`$frontpage_limit`. 500 is a reasonable amount, but more on the
frontpage means more kyrre points. 


### Database Indexes

We open the cockroach sql interface with, f.ex.:
```
cockroach sql --insecure --host=localhost:26257
```

In the sql interface, we select bf as our database:
```
use bf;
```

By grabbing any picture with this command:
```
select picture from users limit 1; 
```

And seeing how the database searches for it with this command,
using the ID gathered from the previous action, such as this:
```
explain SELECT picture FROM pictures WHERE pictureid = 'ebZBY3f5fWg9xOx4R8oBbJ4pHeaN8k.jpg'; 
```

If the returning text now says it spans "FULL SCAN" for this
image, that means the program is unoptimized, and can be fixed
with indexing.

We can make indices for the pictures as they are set up in our
database like this, still in the sql interface:
```
create index on pictures (pictureid); 
```

And make indices for the rest of the keys like this:
```
create index on users (name);
create index on users (userid);
create index on users (lastpostdate);
create index on posts (userid);
create index on posts (postdate);
create index on comments (postid,postdate);
create index on comments (postid); 
```

Here's the improved result. Notice the spans value:

![images/fig-5-1.png](images/fig-5-1.png)
**Fig 5.1:** *Improved image search.*


## 5.3: Docker Image

In our docker VM, we make the file `/etc/docker/daemon.json`,
and write this in it:

```json
{ "insecure-registries" : ["192.168.128.23:5000"] } 
```

We clone
[the bookface repository](https://github.com/hioa-cs/bookface),
and make minor adjustments to the file `Dockerfile` right in the
root of the repository.

One of the adjustments being setting some environment variables
for `config.php.docker`. The values might as well be gathered
from the `config.php` files in www1 and www2. For example:

```
ENV BF_DB_HOST="192.168.132.158"
ENV BF_DB_PORT="26257"
ENV BF_DB_NAME="bf"
ENV BF_DB_USER="bfuser"
ENV BF_DB_PASS=''
ENV BF_WEBHOST='10.212.175.53'
#ENV BF_IMAGE_PATH=
ENV BF_FRONTPAGE_LIMIT=750
#ENV BF_MEMCACHE_SERVER=
```

### Building and running the image

Now we can build the image with this command: the `--tag` tag
setting name and version And the directory being the directory
the Dockerfile is in

```sh
docker build --tag=NAME:VERSION_NAME DIRECTORY
```

![images/fig-5-2.png](images/fig-5-2.png)
**Fig 5.2:** *First time docker building bookface.*

You can see which images you have prepared with this command:

```sh
docker images
```

And run it with this command: `-d` meaning to run in the
background

```sh
docker run -dP NAME:VERSION_NAME
```

And then running containers with this command:

```sh
docker ps
```

There we can also see what ports it's hosting on And see the
site it's hosting with this command: May be it won't be able to
connect to the database, but this is fine.

```sh
curl http://localhost:[PORT]/index.php
```

Also we can use `docker kill` to kill any running containers.


## 5.4: REALLY Hosting Bookface With Docker

We start 2 containers of a bookface image, and move to the load
balancer to modify `/etc/haproxy/haproxy.cfg`. There, we modify
to define servers www1 and www2 under backend "webservers", and
our docker containers under "containers", with the corresponding
IPs and ports:

```
[...]
frontend main
bind *:80
mode http
default_backend containers

backend webservers
balance roundrobin
server www1 192.168.129.2
server www2 192.168.128.103

backend containers
balance roundrobin 
server docker1 192.168.131.56:32777
server docker2 192.168.131.56:32775 
[...]
```

Also remember to restart haproxy for the config changes to have
any effect. We'll probably want to change this back to have
"webservers" as default.


## 5.6: Diagram

![images/fig-5-3.png](images/fig-5-3.png)
**Fig 5.3:** *Diagram to represent our infrastructure.*

Correction: web servers www1 and www2 are on port `26257`.


## 5.10: Docker Instances Are Shut Off When the VM It Runs From is Shut Off

Docker instances are shut off when the VM it runs from is shut
off.


## 5.12: What is a Docker Swarm?

Swarm mode is a way to manage docker instances over a bunch of 
different machines. Maybe even to let them cooperate.


## 5.13: VM Which Installs Docker Automatically

In the "configuration" tab when launching an instance in
Openstack, there's an option to add a script by uploading a file
or typing it into the html field. This is how we can add our
docker installation script for a VM to install docker at
creation.

![images/fig-5-4.png](images/fig-5-4.png)
**Fig 5.4:** *Openstack customization script feature."

By doing this we can install docker on a VM without scp-ing to
it or even ssh-ing to it. We can potentially have a VM which
installs docker and launches a web server without us manually
handling it. There may even be an openstack CLI option which
enables this, which would allow us to make these through
scripts.

