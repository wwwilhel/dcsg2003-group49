# Oblig 2

# Week 8: Backup

## 8.1: Making a Virtual Machine for Backups

![images/fig-8-1.png](images/fig-8-1.png) \
**Fig 8.1:** *We make a virtual machine for backups.*

And copy over manager's private key to db1, to give db1
ssh-access to the backup virual machine.

```sh
scp .ssh/id_rsa ubuntu@[DB1_IP]:.ssh/
```

We make a volume of 5.0GiB, and connect it to `/dev/vdb` on
backup.

And then check if it's alright with this command:

```
ubuntu@backup:~$ sudo sfdisk -l
Disk /dev/loop0: 63.22 MiB, 66293760 bytes, 129480 sectors
[...]

Disk /dev/vdb: 5 GiB, 5368709120 bytes, 10485760 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
ubuntu@backup:~$ 
```


### Mounting It

To do this we have to format to ext4:

```sh
mkfs.ext4 /dev/vdb
```

Now we can mount it to `/mnt`

```sh
mount /dev/vdb /mnt
```

And to confirm it, we can see it located here:

```sh
root@backup:/home/ubuntu# df -h
Filesystem      Size  Used Avail Use% Mounted on
tmpfs            96M  1.1M   95M   2% /run
/dev/vda1        39G  2.1G   37G   6% /
tmpfs           479M     0  479M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/vda15      105M  6.1M   99M   6% /boot/efi
tmpfs            96M  4.0K   96M   1% /run/user/1000
/dev/vdb        4.9G   24K  4.6G   1% /mnt
```

Now the mount command just has to be run every time the backup
server goes down and up again.


## 8.3: Copying Data Over

This is the script we came up with (made to be run as root):

```sh
#!/bin/bash

ARCHIVE="/tmp/db1-copy-$(date "+%g%m%d").tgz"
SOURCE="/bfdata"
BACKUP_IP="192.168.134.46"

# Kills cockroach
killall -SIGINT cockroach

# Makes an archive copy of the data
# Uses "pigz" to utilize all cores
tar -c --use-compress-program="pigz --fast" -f $ARCHIVE $SOURCE

# Starts cockroach up again,
# regardless of if it was on to begin with
/home/ubuntu/bin/start-cockroach

# Transfers the compressed archive over to our backup server
# And removes it
# Needs to use scp as user "ubuntu"
sudo -u ubuntu scp $ARCHIVE $BACKUP_IP:
rm $ARCHIVE
```


### (Errors)

Notably, the script came with these errors upon running:

```
tar: /bfdata/cockroach-temp1118557497: File removed before we read it
tar: /bfdata: file changed as we read it                                                              
``` 


### Shutting Down Cockroach

I felt the need to justify how we shut down our database; this
is how
[cockroachlabs](https://www.baeldung.com/linux/sigint-and-other-termination-signals)
recommends shutting down a cockroach node. 

![images/fig-8-2.png](images/fig-8-2.png) \
**Fig 8.2:** *Cockroach labs telling to shut down nodes with
Ctrl-C.*

Since we prefer running our database in the background, and
besides want to be able to shut it down with unix commands
anyways, we can substitute `Ctrl-C` with this command (just
beware that it would shut down any process initiated with the
command `cockroach` (it's not 100% kosher)):

```sh
killall -SIGINT cockroach
```


## 8.1: The Other Script

```sh
#!/bin/bash

FILES="DCSG2003_V24_group49-openrc.sh .config .ssh docs \
    login mylogs scripts"

ARCHIVE="/tmp/manager-backup-$(date "+%g%m%d").tgz"
BACKUP_IP="192.168.134.46"

cd /home/ubuntu/

tar -c --use-compress-program="pigz --best" -f $ARCHIVE $FILES
sudo -u ubuntu scp $ARCHIVE $BACKUP_IP:
rm $ARCHIVE
```
