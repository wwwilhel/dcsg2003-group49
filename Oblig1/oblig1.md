# Oblig 1: Entering the Uptime Game

# Week 3: Setting up the Four Main Servers

## 3.1-3.4; Setting up 3 new Servers

- We make 3 virtual machines through the SkyHigh interface, as load
  balancer, and 2 web servers.


![fig-1-1](images/fig-1-1.png)

**Fig 1.1:** *Our 4 servers so far, note public IPs on load balancer and
manager.*

- We secure a public floating IP and connect it to the load balancing
  server.
- We install Apache and PHP on our web servers. Using the apt package
  manager requires superuser privileges.
- It's also good practise to run `apt-get update` before using the
  package manager.
```
apt-get install apache2 libapache2-mod-php
```

- We make some simple html-files to test the web servers, and place them in
  the default file location for web hosting (at least for apache I guess):
```
/var/www/html/index.html
```

- We check whether this works through *curl*:
```
curl http://[IP]
```

![fig-1-2](images/fig-1-2.png)
**Fig. 1.2:** *curl returns us the html-file as expected.*

## 3.5: Haproxy

- We install haproxy on the load balancer:
```
apt-get install haproxy net-tools
```

- We edit the config file located at 
```
/etc/haproxy/haproxy.cfg
```

- This just means appending some extra code to the end of the config file. This
  sets the load balancer to round-robin between our web servers.
```
frontend main
bind *:80
mode http
default_backend webservers

backend webservers
balance roundrobin
server www1 192.168.129.2
server www2 192.168.128.103

listen stats
bind *:1936
stats enable
stats uri /
stats hide-version
stats auth someuser:password
```

### When editing Haproxy

- You better check for syntax errors before editing configs and such for haproxy. This command checks for errors:
```
haproxy -c -f /etc/haproxy/haproxy.cfg
```

- You better check for syntax errors before editing configs and such for haproxy. This command checks for errors:
```
haproxy -c -f /etc/haproxy/haproxy.cfg
```

- And these commands restart and apply changes (the start command does nothing if haproxy is already running):
```
service haproxy start
service haproxy restart
```

### Testing if this works

- We check for whether it's online at this point by running this command in the balancer (this setup of flags is convenient to know. I like to think of it as *Link to the Past* or something (Or you can rearrange it to something like `-plant`)):
```
netstat -anltp
```

- We're looking for a line such as this, so grepping for ":80" can be a good idea:
![fig-1-3](images/fig-1-3.png)

**Fig. 1.3:** *Seems Haproxy is doing as it should.*

- Then we test through `curl` or `wget` from the manager server to see whether the load balancer forwards the web servers as expected.

## 3.6: Register to Group 49 in Blackboard

- We register to Group 49 in Blackboard.

## 3.7: The Service User

From the weekly assignments:
> "En viktig del av kurset er å kunne styre de virtuelle maskinenen fra kommandolinjen og å lage små shellscript som automatiserer deler av driften. Vi vil allikevel unngå at dere legger inn deres egne brukernavn og passord inn i disse scriptene, da dette ville blitt synlig for andre i gruppen. I stedet har vi en løsning der hver gruppe har en ekstra bruker, som kan brukes til automatiseringen. Vi kaller denne brukeren "service brukeren" i dette kurset."

### Username and Password for Service User

- The usename is based on our group name, and the password is gained from a file each DCSG2003 user can download from SkyHigh *(Object Store > Containers)*:
```
Username: DCSG2003_V24_group49_service
Password: [REDACTED]
```

- Service is selling, and selling is service.

### The RC File
- This is a shellscript which sets some variables in the shell.
- We log onto SkyHigh as the service user, through "Openstack Accounts", as opposed to "NTNU Accounts",
- and in the tab "API Access", download the openstack RC file.

- Then send the file to **manager** through *scp* (Remember ":" here means directory within the remote machine.):
```
scp SOURCE ununtu@[manager ip]:/home/ubuntu
```

- To avoid entering password when this file runs, we change the first couple of lines:
```
  # With Keystone you pass the keystone password.
# echo "Please enter your OpenStack Password for project $OS_PROJECT_NAME as user $OS_USERNAME: "
# read -sr OS_PASSWORD_INPUT
export OS_PASSWORD="[REDACTED]"
```

- Prompts for the user are commented out and the password is set automatically.


### Downloading the Openstack Client
- We install `python3-openstackclient` through `apt-get`. 
- This has a bunch of dependencies so it can take a while.

- We run the RC file:
```
source DCSG2003_V22_group49-openrc.sh
```

- Now running this command will give an overview of the virtual machines:
```
openstack server list
```

```
+--------------------------------------+----------+--------+----------------------------------------+-------------------------------------------------+----------+
| ID                                   | Name     | Status | Networks                               | Image                                           | Flavor   |
+--------------------------------------+----------+--------+----------------------------------------+-------------------------------------------------+----------+
| f8046c85-fc1f-4b84-a23b-f482d1f69b29 | balancer | ACTIVE | imt3003=10.212.175.53, 192.168.130.144 | Ubuntu Server 22.04 LTS (Jammy Jellyfish) amd64 | gx1.1c1r |
| 0f88c60c-0707-434f-84ab-c0c40cd6ca91 | www1     | ACTIVE | imt3003=192.168.129.2                  | Ubuntu Server 22.04 LTS (Jammy Jellyfish) amd64 | gx1.1c1r |
| 5ede8df6-8609-4d17-a93a-3b09c2ee03d5 | www2     | ACTIVE | imt3003=192.168.128.103                | Ubuntu Server 22.04 LTS (Jammy Jellyfish) amd64 | gx1.1c1r |
| 542526e6-363e-482d-90da-123f0e64ecdb | manager  | ACTIVE | imt3003=10.212.173.107, 192.168.131.76 | Ubuntu Server 22.04 GRID CUDA 12.0              | gx1.1c2r |
+--------------------------------------+----------+--------+----------------------------------------+-------------------------------------------------+----------+
```

- Success!

- We can make this file run automatically by adding a command to run it at the bottom of **manager**'s `~/.bashrc` file:
```
source /home/ubuntu/DCSG2003_V22_group49-openrc.sh
```

# Week 4: Setting up Bookface and the Database

## 4.1: Setting up the Database
- We make a new server as a database. This one is stronger than previous servers.
- We're following the instructions from this site: https://github.com/hioa-cs/bookface

On the new database server, **db1**:

- We install *Cockroach* following these instructions:
https://www.cockroachlabs.com/docs/stable/install-cockroachdb-linux
- Surely there's no reason to rewrite these instructions.

- We expect the version we just installed to be version 23:
```
cockroach version
```

- We make a directory specifically for cockroach, `/bfdata`.
- And start the database with this command:
```
cockroach start \
--insecure \
--store=/bfdata \
--listen-addr=0.0.0.0:26257 \
--http-addr=0.0.0.0:8080 \
--background \
--join=localhost:26257
```

- (We change this command around later, and put it in a shell script for easy access and reference.)
- Then we initialize the database with:
```
cockroach init --insecure --host=localhost:26257
```

- And see if the database is actually running. This can be done f.ex. with `ps aux` and grepping `cockroach`.
- If cockroach is running, there will be a process registered with the name.

### SQL

- Open the cockroach sql interface with:
```
cockroach sql --insecure --host=localhost:26257
```

Using the SQL interface:
- Every command ends with ";", btw.
- This creates the database and defines a user. Note that there's no password. 
```
CREATE DATABASE bf;
CREATE USER bfuser;
GRANT ALL ON DATABASE bf TO bfuser;
```

- This creates the tables we need for Bookface:
```
USE bf;
CREATE table users ( userID INT PRIMARY KEY DEFAULT unique_rowid(), name STRING(50), picture STRING(300), status STRING(10), posts INT, comments INT, lastPostDate TIMESTAMP DEFAULT NOW(), createDate TIMESTAMP DEFAULT NOW());
CREATE table posts ( postID INT PRIMARY KEY DEFAULT unique_rowid(), userID INT, text STRING(300), name STRING(150), image STRING(32), postDate TIMESTAMP DEFAULT NOW());
CREATE table comments ( commentID INT PRIMARY KEY DEFAULT unique_rowid(), postID INT, userID INT, text STRING(300),  postDate TIMESTAMP DEFAULT NOW());
CREATE table pictures ( pictureID STRING(300), picture BYTES );
GRANT SELECT,UPDATE,INSERT on TABLE bf.* to bfuser;
```

- Exit the cockroach SQL interface with `exit;`

### Viewing the CockroachDB Dashboard

From the GitHub repo README.md listed above:

> "As mentioned earlier, an attractive feature of CockroachDB is its web-based dashboard. This provides valuable insight into how the database sees the world. We can see what queries are executed and how long they take.
> 
> The dashboard is available on port 8080, since that was the port chosen above. However, since the database does not (and should not) have a floating IP, it is not available from the outside. One therefore needs to use a tool such as FoxyProxy to browse the dashboard."

### Setting up the Web Servers for Bookface

- We install the necessary dependencies, `apache2 libapache2-mod-php php-pgsql net-tools`.
- We can check whether apache is still running through `ps` or `service apache2 status`.
- We download the bookface code from the git repo, to whatever directory we choose:
```
git clone https://github.com/hioa-cs/bookface.git
```

-  The default index.html should be located in 
```
/var/www/html/
```

- We go ahead and copy the main PHP files into this directory.
- The config file located here is pretty important (From same README):
> "One important part of how bookface works, is it's configuration file. This is where you will specify things like where the database is and the public URL will be. The configuration file is read every time a page is requested from a client and this means you can make dynamic changes to this file in order to change the behaviour of the site without restarting anything. However, it also means that an error in this file will cripple the site. It may be a good idea to come up with a scheme to make sure you have backup and some sort of oversight over this file, especially if you have many webservers."

- We change the contents of the config.php file:
```
<?php 
$dbhost = "192.168.132.158"; 
$dbport = "26257"; 
$db = "bf"; 
$dbuser = "bfuser"; 
$dbpassw = ''; 
$webhost = '10.212.175.53'; 
$weburl = 'http://' . $webhost ; 
$frontpage_limit = 1000; 
?>
```

- At this point we can *curl* the balancer IP and look for this section of html:
```
<table>
<tr><td>Users: </td><td>0</td></tr>
<tr><td>Posts: </td><td>0</td></tr>
<tr><td>Comments: </td><td>0</td></tr>
</table>
```
- The zeroes here imply we have a connection to the database and the site is working as intended.
- This means you can access it from a web browser as well.

## 4.3: Signing up for the Uptime System

- We install the client through this command:
```
curl -s https://raw.githubusercontent.com/kybeg/uc-client/main/install.sh | sudo /bin/bash
```

- And test that we're connected with
```
uc status
```

- We set an endpoint with:
```
uc set endpoint IP
```

- And activate the game with 
```
uc traffic on
```

- More information on the blackboard site under "Informasjon om Oppetidsspillet"
- ???
- Profit.

### -- End of Oblig 1.
