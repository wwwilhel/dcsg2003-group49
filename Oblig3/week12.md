# Week 12: Data Analysis

Much of the text in these files is paraphrased and appropriated
from weekly assignment text. Here it is used as proof of
participation and serves as guide and reference and is better
archived for us as students.


## 12.1: Data Analysis

### 1: All 50.000 measurements

![](pictures/fig-12-1.png) \
**Fig 12.1:** *Frequency distribution spreadsheet of data from
file "tf2_50k.dat"*

The file "tf2\_50k.dat contains 50000 data points of amount of
players in *Team Fortress 2* for some range of time. In the
spreadsheet, we can see that the player amount captured is 451
at the lowest point, and 44876 at the highest point.

The mean is calculated to be 15202, and we can see in the graphs
on the right, that this is about where the peak of each graph
lies.

In the *5% Frequency Distribution Plot*, we see a sort of normal
distribution, centered on about 15202, the mean. This is the
"usual" amount of players, and the surrounding buckets, down to
ca. 4893, and up to ca. 44876, are still within the top 5
percentile, meaning these numbers are yet present in 95% of data
gathered.


### 2: First and last 3.000 measurements

The first data set spanned ca. 173 days of data, so we'll look
at the first and last 3000 measurements to see if there's
significant difference between the two ranges in time.

![](pictures/fig-12-2.png) \
**Fig 12.2:** *First 30000 measurements.*


![](pictures/fig-12-3.png) \
**Fig 12.3:** *Last 30000 measurements.*

We can immediately see a big difference in min and max players
for these ranges in time. From the first 30k to the last,
highest player count jumps from 21056 to 30181, a difference of
over 9000.

Mean player count goes from ca. 13139 to ca. 18822, which means
there *has* been an increase in overall player count, and not
just the variance. Could be the end of this set was after a big
update. 

### 3: Conveying this information to a dum-dum

This dataset contains amount of players in TF2 over about 5
months, where in total, the average player count (total mean) is
about 16209. We also see some spikes of player activity going up
to about 44000 players (max, also visible in the graphs). This
is something we can expect to be true for 90% of the time (the
top 10% percentile) the game is up.

From the start of the time period to the end, there has been an
increase in average player count of ca. 5000 players, and a max
total of 9000. Something went right.


## 12.2: Server Data Analysis

### Using jq

The uptime games has our data stored on a *prometheus* database,
which can be gathered by way of this command, here used to get
value for key "last\_download\_time" (granted by our lecturer
Kyrre Begnum):

```sh
curl -s -g 'http://admin:admin@192.168.132.61:9090/api/v1/query?query=last_download_time{name="group49"}'

# or with package jq:
curl -s -g 'http://admin:admin@192.168.132.61:9090/api/v1/query?query=last_download_time{name="group49"}' | jq
```

Giving this output (here beautified with jq):

```json
{
  "status": "success",
  "data": {
    "resultType": "vector",
    "result": [
      {
        "metric": {
          "__name__": "last_download_time",
          "instance": "192.168.129.65:9001",
          "job": "mongodb",
          "name": "group49",
          "tags": "dcsg2003_24"
        },
        "value": [
          1713045813.538,
          "8.1082"
        ]
      }
    ]
  }
}
```

Or we can use `jq` to get a specific slice; these kinds of tools
are good for automation:

```sh
root@manager:/home/ubuntu# curl -s -g 'http://admin:admin@192.168.132.61:9090/api/v1/query?query=last_download_time{name="group49"}' | jq -r '.data.result[].value[1] '
10.0395
```

### The frontpage\_scale.sh script

We download the script `frontpage_scale.sh` from Kyrre with the
command:

```sh
wget http://10.212.136.60:9001/frontpage_scale.sh
```

With inspection, the script seems to
- Observe download time of the site through the *prometheus*
  database API
    - and then scale up/down the frontpage limit based on our
      download time thresholds
    - It does this through a function with can be "declawed"
      at will, which uses ssh, and does so automatically, to the
      point where it can be used with crontab.


The script could be useful for us, but our server is usually at
a very low frontpage limit and still struggling with download
times.


### Data Analysis

(Note: `sed -e 's/\./,/'` can be piped to change all
instances of `.` to `,`, in case your spreadsheet viewer doesn't
take `.` for decimal.)

```sh
# Data as json
curl -s -g 'http://admin:admin@192.168.132.61:9090/api/v1/query?query=last_download_time{name="group49"}[1h]' | jq

# Purely download times
curl -s -g 'http://admin:admin@192.168.132.61:9090/api/v1/query?query=last_download_time{name="group49"}[1h]' | jq -r '.data.result[] | .values[] | .[1] '

# And for grabbing the 24 last hours
curl -s -g 'http://admin:admin@192.168.132.61:9090/api/v1/query?query=last_download_time{name="group49"}[24h]' | jq -r '.data.result[] | .values[] | .[1] '
```

![](pictures/fig-12-5.png) \
**Fig 12.5:** *Relevant section of the spreadsheet of recent load times.*

We can see that, in the 24 hour-period of recorded data, we had
a maximum loading time of 48.1 seconds, and a minimum of 5.4
seconds. For a minimum, 5.4 seconds is quite high.

![](pictures/fig-12-6.png) \
**Fig 12.6:** *Section showing numbers in the "1% category" with
accumulated probabilities lower than about 90%.*

From fig *12.6*, we see that 11 seconds is the highest loading
time represented under 90% probability, therefore, we can reason
that over 90% of loading times are about 11 seconds or lower.

This is also reflected in the *1% Frequency Distribution Plot*,
visible in fig 12.5. We can estimate that the 90% percentile of
the distribution&mdash; what our site can deliver 90% of the
time, is between 7s and 11s.

