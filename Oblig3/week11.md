# Week 11: Docker Swarm

Much of the text in these files is paraphrased and appropriated
from weekly assignment text. Here it is used as proof of
participation and serves as guide and reference and is better
archived for us as students. 

This week, we'll use the terminology "manager/worker" to discuss
roles in a docker swarm. Don't get in conflated with the manager
of our server family.

## 11.1: Installing Docker

We have to install docker on the three servers now. We got a
script for this earlier, and we get a new one now, and because
they're different, we try and mash them up to install on the
safe side.

And since we need to run this script on all the servers, and
we're too lazy for that, too, we *also* whip up another quick
script to do that for us. We are so opulent.

<ins>my-docker-install-script-2.sh</ins>

```sh
# Run script as superuser.
# Amended for use by tour three beatiful new cluster servers,
# based on new script given in weekly assignment

# Check for conflicting packages
apt-get remove docker.io docker-doc docker-compose \
        docker-compose-v2 podman-docker containerd runc

# Add Docker's official GPG key:
apt-get update
apt-get install -y ca-certificates curl dpkg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add the repository to Apt sources:
add-apt-repository "deb [arch=$(dpkg --print-architecture)] \
        https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) stable"

# Install the Docker Packages
apt-get install -y apt-transport-https containerd.io \
        docker-buildx-plugin docker-ce docker-ce-cli \
        docker-compose-plugin git python3-octaviaclient \
        python3-openstackclient software-properties-common

cat << _EOF_ > /etc/docker/daemon.json
{
"insecure-registries":["192.168.128.23:5000"]
}
_EOF_

systemctl restart docker
docker images
```

<ins>temp-install-script.sh</ins>

```sh
for IP in <ip1> <ip2> <ip3>
do
        echo Installing for $IP !
        scp /home/ubuntu/scripts/my-docker-install-script-2.sh $IP:
        ssh $IP "sudo ./my-docker-install-script-2.sh; cat /etc/docker/daemon.json;"
done
```


## 11.2: Disable Docker on Startup

Docker will start up container instances automatically as part
of a swarm. We don't want this to happen if it means it can do
that before GlusterFS is properly connected. 

We disable the automatic startup, and instead turn it on through
our already established start-up scripts. 

We run these as root on each server:

```sh
systemctl disable docker
systemctl disable docker.service
systemctl disable docker.socket 
```

![Docker not active on server start; success!](pictures/fig-11-1.png) \
**Fig 11.1:** *Docker not active on server start; success!*

And then appended to `start-cockroach` of each server:

```sh
systemctl start docker 
```

![And then we get this result after a reboot with the addition
to the startup script.](pictures/fig-11-2.png) \
**Fig 11.2:** *And then we get this result after a reboot with
the addition to the startup script.*


## 11.3: Making a Docker Swarm

We install first on server1, and this makes this one the
manager&mdash; or "manager" over the other 2 servers&mdash; the
"workers". We can potentially move on to have more managers in
the future.

We run this command on server1 to make it a "Docker Swarm Node"
and also the manager of the swarm.:

```sh 
docker swarm init
```

We get a command for workers to join this swarm. We'll use this
for our workers to join in, but the key we get is random and
maybe important, so we also enter it a place it'll get backed up
to our backup server. 

```sh
docker swarm join --token TOKEN IP:PORT
```

We can also get the key back by running `docker swarm join-token
worker` on the manager server again. Or run `docker swarm join-token
manager` to assign as manager.

The way I understand, this terminology has gone from
"master/slave" to "manager/worker", which I would think is
easier to uphold in a professional enviromnent, but the
transition brings to mind quite a bleak image.


## 11.4: Cloning the Bookface Repo on server1

We clone the bookface Repo on server1.


## 11.5: Making a Haproxy Config File

In this architecture, the haproxy load balancer runs as its own
Docker service. This is where the web servers now go to reach
the databases. We make a `haproxy.cfg` file in server1,
and&mdash; since it needs to be available on all servers, we can
make it so Docker supply it for all the servers in the swarm.

```
global
    log         127.0.0.1 local2

    pidfile     /tmp/haproxy.pid
    maxconn     4000

defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s 
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s 
    timeout check           10s
    maxconn                 3000

listen stats
    bind *:1936
    stats enable
    stats uri /
    stats hide-version
    stats auth someuser:password


frontend  db  
    bind *:26257
    mode tcp 
    default_backend databases

backend databases
    mode tcp 
    balance     roundrobin

    server      server1 <IP1>:26257
    server      server2 <IP2>:26257
    server      server3 <IP3>:26257
```


## 11.6: Starting Bookface With the New Architecture

In this architecture, our service will be started with a
`docker-compose.yml`-file, thus starting memcache and haproxy in
addition to bookface-containers. 

Nice thing is that since these processes are shared between the
three servers, it's nice and redundant. 

We can inspect this file, and find some useful information. We
see the `haproxy.cfg` file we made used as configuration as
expected. We see port **80** exposed for the web service, and port
**1936** used by the load balancer. And we see the network our
containers are part of is **bf**. 

Now we start bookface:

```sh
docker stack deploy -c docker-compose.yml bf  # Launching Bookface
```

And we can inspect the docker processes like this:

```sh
docker service ls            # Viewing running processes
docker service ps <PROCESS>  # Inspecting a service closer
```

![Inspecting the Docker processes.](pictures/fig-11-3.png) \
**Fig 11.3:** *Inspecting the Docker processes.*


## 11.7: 

We can import the bookface data from the previous iteration, we
just gotta specify where to get the data from.

### Book III SQL: Amendment and Key

First, there was a mistake in our initial iteration; the table
"posts" didn't have a column for pictures. Let's mend this in
the new cluster:

```sql
use bf;

drop table posts;
CREATE table posts ( postID INT PRIMARY KEY DEFAULT unique_rowid(),
    userID INT, text STRING(300), name STRING(150),
    image STRING(32), postDate TIMESTAMP DEFAULT NOW());
GRANT SELECT,UPDATE,INSERT on TABLE bf.* to bfuser;
```

We also need a secret key for the data migration, and this is
implemented through SQL. We enter our secret key here, after the
song by Herbie Hancock:

```sql
insert into config ( key, value ) values ( 'migration_key', 'butterfly' );
```


### Importation

We need to import from a newer version of Bookface that is on
our old architecture. Upgrading the whole thing would take a
while, so we can start it as a container and import through
this. 

This is done with this long command, run from server1 (values can be found in the
old `config.php` files):

```sh
docker run -d --name=bf-import-helper -p 20080:80 -e \
    BF_DB_HOST=<OUTDATED_DB_IP> -e \
    BF_DB_PORT=26257 -e \
    BF_DB_USER=bfuser -e \
    BF_MEMCACHE_SERVER=<OUTDATED_MEMCACHE_IP> -e \
    BF_DB_NAME=bf \
    192.168.128.23:5000/bf:v17
```

We can see that *bf-import-helper* is running with `docker ps`,
and also that it's hosting our old Bookface with `curl
localhost:20080`, right on server1.

Then we start the migration with this command, with the
entrypoint being Balancer's private IP:

```sh
curl -s "http://localhost/import.php?entrypoint=192.168.130.144&key=butterfly"
```

![End of the really long import process.](pictures/fig-11-4.png) \
**Fig 11.4:** *End of the really long import process.*


## 11.8: Using OpenStack as a Load Balancer

We use OpenStack's API for load balancing between a floating-IP
and our three servers.

We do this step through our "manager" server, and need to
install package `python3-octaviaclient`, and copy over the
**RC**-file from *manager* to *server1*. We put a command to
source the file in server1's `.bashrc`.

We make a load balancer within OpenStack, and check its status:

```sh
openstack loadbalancer create --name bflb \
    --vip-subnet-id c3ea9f88-8381-46b0-80e0-910c676a0fbd 
```

```
$ openstack loadbalancer show bflb  # Investigate load balancer 
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| admin_state_up      | True                                 |
| availability_zone   | None                                 |
| created_at          | 2024-04-12T02:08:10                  |
| description         |                                      |
| flavor_id           | None                                 |
| id                  | 10daa52a-7fc7-4a2c-a242-545c814ef528 |
| listeners           |                                      |
| name                | bflb                                 |
| operating_status    | OFFLINE                              |
| pools               |                                      |
| project_id          | 174caf25fd844bfea4bae34f81bfa056     |
| provider            | amphora                              |
| provisioning_status | PENDING_CREATE                       |
| updated_at          | None                                 |
| vip_address         | 192.168.134.234                      |
| vip_network_id      | 35fe8b76-cc31-4b48-b3f0-5c48eef8d289 |
| vip_port_id         | e2611b4a-cdb9-4fa8-a075-5e48742ae7b0 |
| vip_qos_policy_id   | None                                 |
| vip_subnet_id       | c3ea9f88-8381-46b0-80e0-910c676a0fbd |
| tags                |                                      |
+---------------------+--------------------------------------+
```

We have to wait for the "probisioning_status" field to read
"ACTIVE" before we proceed. And we make a listener, and also a
pool:

```sh
openstack loadbalancer listener create --name bflistener \
    --protocol HTTP --protocol-port 80 bflb 
```

```sh
openstack loadbalancer pool create --name bfpool \
    --lb-algorithm ROUND_ROBIN --listener bflistener --protocol HTTP 
```

Then we add server{1,2,3} as members of the pool, by running
this command for each server IP:


```sh
openstack loadbalancer member create --address <SERVER_IP> \
    --protocol-port 80 bfpool 
```

Now we need a new floating IP for the load balancer, so we can
go live with our new architecture:

```sh
$ openstack floating ip create --description bf_swarm_ip 730cb16e-a460-4a87-8c73-50a2cb2293f9 
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| created_at          | 2024-04-12T02:18:34Z                 |
| description         | bf_swarm_ip                          |
| dns_domain          | None                                 |
| dns_name            | None                                 |
| fixed_ip_address    | None                                 |
| floating_ip_address | 10.212.174.195                       |
| floating_network_id | 730cb16e-a460-4a87-8c73-50a2cb2293f9 |
| id                  | 3ccf9044-ca8d-4b64-9ebd-02749fa23986 |
| name                | 10.212.174.195                       |
| port_details        | None                                 |
| port_id             | None                                 |
| project_id          | 174caf25fd844bfea4bae34f81bfa056     |
| qos_policy_id       | None                                 |
| revision_number     | 0                                    |
| router_id           | None                                 |
| status              | DOWN                                 |
| subnet_id           | None                                 |
| tags                | []                                   |
| updated_at          | 2024-04-12T02:18:34Z                 |
+---------------------+--------------------------------------+
```

And it returns an IP for us to use in the field
"floating_ip_address". We'll also need "id" for when we connect
it:

```sh
openstack floating ip set --port \
    e2611b4a-cdb9-4fa8-a075-5e48742ae7b0 \ # load balancer "vip_port_id"
    3ccf9044-ca8d-4b64-9ebd-02749fa23986   # floating ip "id"
```

![Looks like our site is available through the new floating IP.](pictures/fig-11-5.png) \
**Fig 11.5:** *Looks like our site is available through the new
floating IP.*


## 11.9: Going Live

![New endpoint set.](pictures/fig-11-6.png) \
**Fig 11.6:** *New endpoint set.*

### Time for a Promotion

From server1, run this command and all servers are manager:

```sh
docker node promote server2 server3
```
