# Week 10: Shared Storage

## 10.1: Installing GlusterFS

For all servers:

We install GlusterFS on the servers, these commands activate the
service and let it active:

```sh
apt-get -y install glusterfs-server glusterfs-client
systemctl enable glusterd
systemctl start glusterd
```

![](pictures/fig-10-1.png) \
**Fig 10.1:** *gluster successfully set up.*


## 10.2: Bricks

For all servers:

We use "bricks", files with binary data for the sake of
distributed storage between the servers. We make directories for
storing pictures, configuration, and two for mounting the
distributed file systems.

```sh
mkdir /bf_brick
mkdir /config_brick

mkdir /bf_images
mkdir /bf_config
```

Or more automatic: 

```sh
ssh 192.168.131.131 "sudo su; \
    mkdir --mode=754    /bf_brick /config_brick /bf_images /bf_config; \
    chown ubuntu:ubuntu /bf_brick /config_brick /bf_images /bf_config;"

ssh 192.168.128.132 "sudo su; \
    mkdir --mode=754    /bf_brick /config_brick /bf_images /bf_config; \
    chown ubuntu:ubuntu /bf_brick /config_brick /bf_images /bf_config;"

ssh 192.168.132.166 "sudo su; \
    mkdir --mode=754    /bf_brick /config_brick /bf_images /bf_config; \
    chown ubuntu:ubuntu /bf_brick /config_brick /bf_images /bf_config;"

ssh 192.168.131.131 ls -l /
ssh 192.168.128.132 ls -l /
ssh 192.168.132.166 ls -l /
```

![](pictures/fig-10-2.png) \
**Fig 10.2:** *The directories are in place.*


## 10.3: Synchronizing with gluster

For server1:

We gotta connect the three servers to the cluster through
gluster:

```sh
gluster peer probe 192.168.128.132    # server2
gluster peer probe 192.168.132.166    # server3
gluster peer status
```

![](pictures/fig-10-3.png) \
**Fig 10.3:** *Synchronized through gluster.*


Note these commands for managing gluster (and
[here's](https://lzone.de/#/LZone%20Cheat%20Sheets/DevOps%20Services/GlusterFS)
a gluster cheat sheet):

```sh
gluster volume status
gluster volume status bf_images
gluster peer status
```


## 10.4: Making Replica Volumes in Gluster

For server1:

We make volumes on `/bf_images` and `/bf_config`.

```sh
gluster volume create bf_images replica 3 \
    192.168.131.131:/bf_brick \       # server1
    192.168.128.132:/bf_brick \       # server2
    192.168.132.166:/bf_brick force   # server3

gluster volume start bf_images

gluster volume create bf_config replica 3 \
    192.168.131.131:/config_brick \       # server1
    192.168.128.132:/config_brick \       # server2
    192.168.132.166:/config_brick force   # server3

gluster volume start bf_config

df  # Check whether the volumes are correctly mounted
```

![](pictures/fig-10-4.png) \
**Fig 10.4:** *We've created the volumes.*

## 10.5: Mounting the File Systems

On all three servers:

```sh
mount -t glusterfs [CURRENT_IP]:bf_config /bf_config
mount -t glusterfs [CURRENT_IP]:bf_images /bf_images 
```

And we can check again with command:

```sh
dh | grep /bf
```

With this done, anything put in a directory on one server, will
be available from the other servers as well.

![](pictures/fig-10-5.png) \
**Fig 10.5:** *Directories are mounted, the permissions are
correct, and the files are shared between the servers.*


## 10.6: Automatic Mount on Boot

On all three servers:

At this point, if a server goes down, it's automatically started
up again, and glusterd is too; also glusterd. But the
directories won't be remounted. We make a file
`/bf_config/is_mounted` and have our "start-cockroac" script--
which runs on server boot, check it.

We also let it write to our discord channel for when the servers
boot and mount. 

Here's the new "start-cockroach" script:

```sh
#!/bin/bash

# Declarations
URL=[DISCO WEBHOOK URL]
MYNAME="$HOSTNAME.log"

# Start mount-check-loop
mount -t glusterfs 192.168.131.131:bf_config /bf_config
sleep 2s
while [ ! -f /bf_config/is_mounted ];
do
        sleep 2s

        message="NOW MOUNTING..." 
        JSON="{\"username\": \"$MYNAME\", \"content\": \"$message\"}"
        curl -s -X POST -H "Content-Type: application/json" -d "$JSON" $URL

        mount -t glusterfs 192.168.131.131:bf_config /bf_config
done
mount -t glusterfs 192.168.131.131:bf_images /bf_images

message="$HOSTNAME IS MOUNTED!"
JSON="{\"username\": \"$MYNAME\", \"content\": \"$message\"}"
curl -s -X POST -H "Content-Type: application/json" -d "$JSON" $URL

[...]
```

Here's how it looks in our discord channel:

![](pictures/fig-10-6.png) \
**Fig 10.6:** *Great!*
