#!/bin/bash

# Uses the test command
if test -f $1; then
    echo "It's real. :-)"
else
    echo "It's not real! (╯°□°)╯︵ ┻━┻"
fi

# Uses the "ls" command itself and its return value
# This will still print error messages, which is unfortunate
if ls $1 > /dev/null; then echo "Alright"; else echo "Not OK"; fi
